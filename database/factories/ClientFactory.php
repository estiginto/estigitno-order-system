<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Client;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_name' => $this->faker->word,
            'company_tax_id' => $this->faker->word,
            'company_address' => $this->faker->word,
            'contact_name' => $this->faker->word,
            'contact_phone' => $this->faker->word,
            'contact_email' => $this->faker->word,
            'contact_address' => $this->faker->word,
            'memo' => $this->faker->text,
        ];
    }
}
