<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\ServiceItem;

class ServiceItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServiceItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sku' => $this->faker->regexify('[A-Za-z0-9]{20}'),
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'price' => $this->faker->numberBetween(-10000, 10000),
            'minimum_price' => $this->faker->numberBetween(-10000, 10000),
            'is_include_tax' => $this->faker->boolean,
            'memo' => $this->faker->text,
            'internal_memo' => $this->faker->text,
        ];
    }
}
