<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_items', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 20)->unique()->index();
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('price');
            $table->integer('minimum_price')->nullable();
            $table->boolean('is_include_tax');
            $table->text('memo')->nullable();
            $table->text('internal_memo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_items');
    }
}
